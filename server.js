const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const users = require('./users.json');
const path = require('path');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const maps = require('./maps.json');

require('./passport.config');

server.listen(3000);

let mapId;

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/login', (req, res) => {
    res.sendFile(path.join(__dirname, 'login.html'));
});

app.get('/game',  (req, res) => {
    res.sendFile(path.join(__dirname, 'game.html'));
});

app.post('/login', function (req, res) {
    const userFromReq = req.body;
    const userInDB = users.find(user => user.login === userFromReq.login);
    if (userInDB && userInDB.password === userFromReq.password) {
      const token = jwt.sign(userFromReq, 'someSecret');
      res.status(200).json({ auth: true, token });
    } else {
      res.status(401).json({ auth: false });
    }
});

app.post('/game', passport.authenticate('jwt', {session: false}), function (req, res) {
  const currMap = maps.find(map => map.id === mapId);
  res.json({ currMap, textId: mapId });
});


let players = 0, timer, gameStatus = 'off', playersArr = [];
let endOfGame;

io.on('connection', socket => {
    players++;
    console.log(`players: ${players}`);
    console.log(gameStatus);
     
    const startGame = () => {
      playersArr = [];
      mapId = getRandomInt(3);
      io.to('wait').emit('quit room');
      io.to('play').emit('start timer');
      io.to('play').emit('start game');
      let now = new Date();
      endOfGame = now.getTime() + 65000;
    };

    if (gameStatus == 'on') {
      socket.join('wait');
      let now = new Date();
      socket.emit('timer for wait-room', { time: endOfGame - now.getTime() });
      console.log('user joined wait room');
    } else {
      socket.join('play');
      if (players == 1) {
        timer = setInterval(startGame, 75000);
      }
    }

    socket.on('change game status', payload => {

      gameStatus = payload.status;
      console.log(gameStatus);

    }); 

    socket.on('new user', payload => {

      const { token } = payload;
      const user = jwt.verify(token, 'someSecret');

      if(user) {
        const userLogin = user.login;
        playersArr.push({ user: userLogin, progress: 0, time: 0, id: socket.id });

        io.to('play').emit('new player', { user: userLogin });
      }

    });

    socket.on('join game', payload => {

      const { token } = payload;
      const user = jwt.verify(token, 'someSecret');
      if (user) {
        const userLogin = user.login;
        socket.leave('wait');
        socket.join('play');
        console.log(`${userLogin} left wait-room`);
      }
      
    });

    socket.on('confirm correct symbol', payload => {

      const { token } = payload;
      const user = jwt.verify(token, 'someSecret');
      if (user) {
        const userLogin = user.login;

        for (let key in playersArr) {
          if (playersArr[key].user == userLogin) 
          playersArr[key].progress++;
        }

        io.emit('correct symbol', { user: userLogin });
      }

    });

    socket.on('user finished the text', payload => {

      const { token } = payload;
      const user = jwt.verify(token, 'someSecret');

      if (user) {
        const userLogin = user.login;
        let now = new Date();
        let time = 60000 - endOfGame + now.getTime();
        for (let key in playersArr) {
          if (playersArr[key].user == userLogin) 
          playersArr[key].time = time;
        }
      }

    });

    socket.on('the end', () => {

      playersArr.sort(compareProgress);
      console.log(playersArr);
      socket.emit('results', { players: playersArr });

    });

    socket.on('disconnect', () => {
      players--;
      for (let key in playersArr) {
        if (playersArr[key].id == socket.id) {
          io.to('play').emit('player disconnected', { user: playersArr[key].user });
          playersArr.splice(key, 1);
        }
      }
      delete playersArr[socket.id];
      if (players == 0) {
        clearInterval(timer);
        gameStatus = 'off';
      }
      console.log(`players: ${players}`);
    });

    

});

function getRandomInt(max) {

  return Math.floor(Math.random() * Math.floor(max));

}

function compareProgress(a, b) {

  if (b.progress == a.progress) return a.time - b.time;
  return b.progress - a.progress;
  
}





