    window.onload = () => {

    const jwt = localStorage.getItem('jwt');
    if (!jwt) {
        location.replace('/login');
    } else {
        const textField = document.querySelector('#text-field');
        const enteredText = document.querySelector('#entered');
        const resultDiv = document.querySelector('#results');
        const timer = document.querySelector('#time');
        const timerLabel = document.querySelector('#timer');
        const winners = document.querySelector('#winners');
        const information = document.querySelector('#information');

        let currTextLength;

        const socket = io.connect('http://localhost:3000');

        const gameProcess = (ev) => {

            let symbol = getChar(ev)
            let symbolsArr = textField.innerHTML.split('');
            let entered = enteredText.innerHTML;

            if (symbol == symbolsArr[0]) {
                socket.emit('confirm correct symbol', { token: jwt });
                entered = entered + symbol;
                symbolsArr.shift();

                if (symbolsArr.length == 0) 
                socket.emit('user finished the text', { token: jwt });

                textField.innerHTML = symbolsArr.join('');
                enteredText.innerHTML = entered;
            }

        }


        socket.on('start game', () => {

            console.log('start of the game');
            winners.innerHTML = '';
            socket.emit('change game status', { status: 'on' });

            fetch('/game', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                     Authorization: `Bearer ${jwt}`
                }
            }).then(res => {
                res.json().then(body => {
                    textField.innerHTML = body.currMap.text;
                    currTextLength = body.currMap.text.length;
                    socket.emit('new user', { token: jwt });
                })
            })

            setTimeout(() => {
                document.addEventListener('keypress', gameProcess);
                startTimer(60, timer);
                timerLabel.innerHTML = 'the race ends in: ';
            }, 5000);

            setTimeout(() => {
                document.removeEventListener('keypress', gameProcess);
                socket.emit('change game status', { status: 'off' });
                textField.innerHTML = '';
                enteredText.innerHTML = '';
                timer.innerHTML = 0;
                timerLabel.innerHTML = 0;
                information.innerHTML = '';
                socket.emit('the end');

            }, 65000);

        });

        socket.on('new user', () => {

            socket.emit('new user in game', { token: jwt });

        });

        socket.on('new player', payload => {

            const playerInfo = document.getElementsByClassName(payload.user)[0];
            if (playerInfo) {
                playerInfo.remove();
            }
            const ProgBar = document.createElement('progress');
            const label = document.createElement('span');
            const userResults = document.createElement('div');
            userResults.className = payload.user;
            label.innerHTML = payload.user;
            ProgBar.value = 0;
            ProgBar.max = currTextLength;
            ProgBar.id = payload.user;
            userResults.appendChild(ProgBar);
            userResults.appendChild(label);
            resultDiv.appendChild(userResults);

        });

        socket.on('correct symbol', payload => {

            console.log(`${payload.user} entered correct symbol`);
            const progressBar = document.getElementById(payload.user);
            progressBar.value += 1;

        });

        socket.on('results', payload => {

            for (let key in payload.players) {
                let place = document.createElement('li');
                place.innerHTML = payload.players[key].user;
                winners.appendChild(place);
            }

        });

        socket.on('player disconnected', payload => {

            const userResults = document.querySelector(`.${payload.user}`);
            userResults.remove();
            information.innerHTML = `${payload.user} got in an accident.`;

        });

        socket.on('quit room', () => {

            socket.emit('join game', { token: jwt });

        });

        socket.on('start timer', () => {

            startTimer(5, timer);
            timerLabel.innerHTML = 'the race starts in: ';

        });

        socket.on('timer for wait-room', payload => {

            let time = Math.round(payload.time/1000);
            startTimer(time, timer);
            timerLabel.innerHTML = 'the race ends in: '

        });
    }
}   


function getChar(event) {

    if (event.which == null) { 
      if (event.keyCode < 32) return null;
      return String.fromCharCode(event.keyCode)
    }
  
    if (event.which != 0 && event.charCode != 0) { 
      if (event.which < 32) return null; 
      return String.fromCharCode(event.which); 
    }
    return null; 

  }

  
  
  function startTimer(duration, display) {

    var start = Date.now(),
        diff,
        minutes,
        seconds;
    function timer() {
        
        diff = duration - (((Date.now() - start) / 1000) | 0);
  
        minutes = (diff / 60) | 0;
        seconds = (diff % 60) | 0;
  
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
  
        display.textContent = minutes + ":" + seconds; 
  
        if (diff <= 0) {
            
            start = Date.now() + 1000;
        }

    };
    timer();
    let interval = setInterval(timer, 1000);
    setTimeout(() => {
        clearInterval(interval);
    }, duration*1000);

}